package com.classpath.spring.commute;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Passenger {
	
	private Driver driver;

	@Autowired
	/*
	 * by name
	 * by type
	 * by constuctor
	 * auto detect
	 */
	
	//constructor based dependency injection
	//setter based dependency injection
	public Passenger(Driver driver) {
		super();
		this.driver = driver;
	}
	
	
	public void commute(String from, String to) {
		this.driver.trip(from, to);
	}
	

}
