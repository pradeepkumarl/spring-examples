package com.classpath.spring.commute;

import org.springframework.stereotype.Component;

@Component
public class Driver {
	
	public void trip(String from, String to) {
		System.out.println("Travelling from "+from +" to"+ to );
	}

}
