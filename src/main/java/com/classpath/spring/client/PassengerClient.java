package com.classpath.spring.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.classpath.spring.commute.Passenger;

@Component
public class PassengerClient implements CommandLineRunner{

	private final ApplicationContext applicatinContext;
	
	@Autowired
	public PassengerClient(ApplicationContext applicationContext) {
		this.applicatinContext = applicationContext;
	}
	@Override
	public void run(String... args) throws Exception {
		Passenger passenger = this.applicatinContext.getBean("passenger", Passenger.class);
		passenger.commute("BTM", "Koramangala");
		
	}
	

}
